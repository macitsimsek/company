<?php

namespace App\Jobs;

use App\Company;
use App\QueuedCompany;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Date;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class NewCompanyQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $count;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 12000;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 10;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($count)
    {
        $this->count = $count;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count = $this->count;

        $companyNumber = DB::table('queued_companies')
        ->select('companyNumber')
        ->orderBy('companyNumber', 'desc')
        ->limit(1)
        ->pluck('companyNumber')
        ->first();
        
        for ($a=$companyNumber+1; $a<=$count; $a++) {
            try {
                $newCompanyQueue = new QueuedCompany();
                $newCompanyQueue->companyNumber = $a;
                $newCompanyQueue->done=0;
                $newCompanyQueue->save();
                logger($a . " Company number queued");
            } catch (\Throwable $th) {
            }
        }

        //logger($count . " Company fetching finished");
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['new', 'company'];
    }
}
