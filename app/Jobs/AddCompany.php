<?php

namespace App\Jobs;

use App\Company;
use App\QueuedCompany;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Date;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class AddCompany implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $count;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 12000;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 10;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($count)
    {
        $this->count = $count;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count = $this->count;

        AddCompany::dispatch($count);

        $queuedCompanies = QueuedCompany::where('done', '0')
            ->limit($count)
            ->orderBy('companyNumber', 'asc')
            ->get();

        if($queuedCompanies->count()==0){
            DB::table('queued_companies')
                ->where('done', '=', "1")
                ->update(array('done' => "0"));
        }

        logger($count . " Company selected");
        DB::table('queued_companies')
        ->where('done', '=', "0")
        ->limit($count)
        ->orderBy('companyNumber', 'asc')
        ->update(array('done' => "1"));
        logger($count . " Company updated");

        foreach ($queuedCompanies as $queuedCompany) {
            try {
                logger($queuedCompany->companyNumber . " company fetching started");
                $client = new Client();
                $url = 'http://data.companieshouse.gov.uk/doc/company/' . $queuedCompany->companyNumber . '.json';
                $res = $client->request('GET', $url, [
                    'headers' => [
                        'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
                        'Accept'     => 'application/json',
                        'Content-type' => 'application/json'
                    ]
                ]);

                $result = json_decode($res->getBody());

                if (!isset($result->primaryTopic)) {
                    $queuedCompany->done = "4";
                    $queuedCompany->timestamps = false;
                    $queuedCompany->save();
                    return 0;
                }

                $result = $result->primaryTopic;

                $company = Company::where('companyNumber', $queuedCompany->companyNumber)->first();

                if (!$company) {
                    Company::insert(
                        [
                            'companyNumber'             => $result->CompanyNumber,
                            'companyName'               => $result->CompanyName,
                            'addressLine1'              => isset($result->RegAddress->AddressLine1) ? $result->RegAddress->AddressLine1 : null,
                            'addressLine2'              => isset($result->RegAddress->AddressLine2) ? $result->RegAddress->AddressLine2 : null,
                            'postTown'                  => isset($result->RegAddress->PostTown) ? $result->RegAddress->PostTown : null,
                            'country'                   => isset($result->RegAddress->Country) ? $result->RegAddress->Country : null,
                            'postcode'                  => isset($result->RegAddress->Postcode) ? $result->RegAddress->Postcode : null,
                            'companyCategory'           => isset($result->CompanyCategory) ? $result->CompanyCategory : null,
                            'companyStatus'             => isset($result->CompanyStatus) ? $result->CompanyStatus : null,
                            'countryOfOrigin'           => isset($result->CountryOfOrigin) ? $result->CountryOfOrigin : null,
                            'incorporationDate'         => isset($result->IncorporationDate) ? Carbon::createFromFormat('d/m/Y', $result->IncorporationDate) : null,
                            'accountRefDay'             => isset($result->Accounts->AccountRefDay) ? $result->Accounts->AccountRefDay : null,
                            'accountRefMonth'           => isset($result->Accounts->AccountRefMonth) ? $result->Accounts->AccountRefMonth : null,
                            'accountsNextDueDate'       => isset($result->Accounts->NextDueDate) ? Carbon::createFromFormat('d/m/Y', $result->Accounts->NextDueDate) : null,
                            'accountsLastMadeUpDate'    => isset($result->Accounts->LastMadeUpDate) ? Carbon::createFromFormat('d/m/Y', $result->Accounts->LastMadeUpDate) : null,
                            'accountCategory'           => isset($result->Accounts->AccountCategory) ? $result->Accounts->AccountCategory : null,
                            'returnsNextDueDate'        => isset($result->Returns->NextDueDate) ? Carbon::createFromFormat('d/m/Y', $result->Returns->NextDueDate) : null,
                            'returnsLastMadeUpDate'     => isset($result->Returns->LastMadeUpDate) ? Carbon::createFromFormat('d/m/Y', $result->Returns->LastMadeUpDate) : null,
                            'sicText'                   => isset($result->SICCodes->SicText) ? json_encode($result->SICCodes->SicText) : null
                        ]
                    );
                }
                $queuedCompany->done = "2";
                $queuedCompany->timestamps = false;
                $queuedCompany->save();
                logger($queuedCompany->companyNumber . " company fetching finished");
            } catch (\Throwable $th) {
                $queuedCompany->done = "3";
                $queuedCompany->timestamps = false;
                $queuedCompany->save();
            }
        }

        logger($count . " Company fetching finished");
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['fetch', 'company'];
    }
}
