<?php

namespace App\Console\Commands;

use App\Jobs\AddCompany;
use App\Jobs\NewCompanyQueue;
use App\QueuedCompany;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddCompanyQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:company {count?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add company numbers to queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = $this->argument('count');
        NewCompanyQueue::dispatch($count);
    }
}
