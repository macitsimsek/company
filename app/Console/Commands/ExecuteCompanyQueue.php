<?php

namespace App\Console\Commands;

use App\Jobs\AddCompany;
use App\QueuedCompany;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExecuteCompanyQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:company {count?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch the companies from the api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = $this->argument('count');
        AddCompany::dispatch($count);
    }
}
