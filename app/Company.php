<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'companyNumber',
        'companyName',
        'addressLine1',
        'addressLine2',
        'postTown',
        'country',
        'postcode',
        'companyCategory',
        'companyStatus',
        'countryOfOrigin',
        'incorporationDate',
        'accountRefDay',
        'accountRefMonth',
        'accountsNextDueDate',
        'accountsLastMadeUpDate',
        'accountCategory',
        'returnsNextDueDate',
        'returnsLastMadeUpDate',
        'sicText',
    ];
}
