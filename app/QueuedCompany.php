<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueuedCompany extends Model
{
    protected $table = 'queued_companies';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'companyNumber',
        'done',
        'createdAt',
        'updatedAt'
    ];
}
