<?php

use Illuminate\Database\Seeder;

class QueuedCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* $a = 12320100; */
        $a = 12341101;
        for ($i=12320101; $i < $a; $i++) {
            DB::table('queued_companies')->insert([
                    'companyNumber' => $i,
                    'done' => 0
                ]);
            }
        }
}
