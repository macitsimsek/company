<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->integer('companyNumber')->unsigned()->unique();
            $table->string('companyName')->nullable();
            $table->string('addressLine1')->nullable();
            $table->string('addressLine2')->nullable();
            $table->string('postTown')->nullable();
            $table->string('country')->nullable();
            $table->string('postcode')->nullable();
            $table->string('companyCategory')->nullable();
            $table->string('companyStatus')->nullable();
            $table->string('countryOfOrigin')->nullable();
            $table->date('incorporationDate')->nullable();
            $table->string('accountRefDay')->nullable();
            $table->string('accountRefMonth')->nullable();
            $table->date('accountsNextDueDate')->nullable();
            $table->string('accountsLastMadeUpDate')->nullable();
            $table->string('accountCategory')->nullable();
            $table->date('returnsNextDueDate')->nullable();
            $table->date('returnsLastMadeUpDate')->nullable();
            $table->longText('sicText')->nullable();
            $table->timestamp('createdAt')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
