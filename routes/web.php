<?php

use App\Company;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Jobs\AddCompany;
use App\QueuedCompany;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $count = Company::count();
    $last_companies = Company::where(['companyStatus'=>'Active'])->orderBy('id', 'desc')->take(20)->get();
    return [
        "company_count" => $count,
        "last_companies" => $last_companies
    ];
});

Route::get('/new/{count?}', function ($count = 10) {
    for ($i=0; $i < $count; $i++) {
        dispatch(new AddCompany(1000));
    }
    return $count. " job created.";
});


Route::get('/start/{count?}', function ($count = 500) {
    $ids = QueuedCompany::where('done', '0')
        ->limit($count)
        ->orderBy('companyNumber', 'asc')
        ->get();
    foreach ($ids as $id) {
        dispatch(new AddCompany($id));
    }
    return ["started"];
});
